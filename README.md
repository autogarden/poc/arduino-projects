# Arduino projects

## Libraries used

* https://github.com/adafruit/DHT-sensor-library/archive/master.zip
* https://github.com/adafruit/Adafruit-BMP085-Library/archive/master.zip
* https://github.com/adafruit/Adafruit_Sensor/archive/master.zip

List of de libraries directory

* Adafruit_ADS1X15-1.1.1
* Adafruit_BMP085_Library
* Adafruit-BMP085-Library-master
* Adafruit_BMP085_Unified-master
* Adafruit_BusIO
* Adafruit_Circuit_Playground
* Adafruit_GFX_Library
* Adafruit_GPS_Library
* Adafruit_ILI9341
* Adafruit_LED_Backpack_Library
* Adafruit_Sensor-master
* Adafruit_SleepyDog_Library
* Adafruit_STMPE610
* Adafruit_TouchScreen
* Adafruit_Zero_DMA_Library
* Adafruit_Zero_FFT_Library
* Adafruit_Zero_PDM_Library
* async-mqtt-client-master
* BMP180_Breakout_Arduino_Library-master
* DHT-sensor-library-master
* ESPAsyncTCP-master
* PubSubClient-2.8.0
* RTClib
* Servo
* TinyWireM
* WaveHC

